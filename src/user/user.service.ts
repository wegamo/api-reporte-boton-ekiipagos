import { UserI } from '../interfaces/user.interface';
import { User } from '../model/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Repository, UpdateResult, DeleteResult } from 'typeorm';
import { HANDLER_RESPONSE } from 'src/utils/handler-response.utils';
import { sendEmail } from '../utils/send-email';
import { forgotPasswordTemplate } from '../utils/forgotPasswordTemplate';

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(User)
        private readonly userModel: Repository<User>,
    ) { }

    public async find(): Promise<User[]> {
        return this.userModel.find({ relations: ['id_role'], order: { id: "ASC" } })
    }

    public async findOne(data: any): Promise<User> {

        return this.userModel.findOne({ where: data, relations: ['id_role'] });
    }

    public async create(data: UserI): Promise<User> {
        return this.userModel.save(data);
    }

    public async update(id, data: UserI): Promise<UpdateResult> {
        return this.userModel.update({ id }, data)

    }

    public async delete(id: number): Promise<DeleteResult> {
        return this.userModel.delete({ id })
    }
    public async recoverPassword(login: any): Promise<any> {

        const user = await this.findOne({ login });

        if (!user) {
            throw new HttpException('El usuario no está registrado.', HttpStatus.BAD_REQUEST);
        }
        const subject = 'Ekiipago - Recuperación de contraseña';
        const html = forgotPasswordTemplate(user.name, user.password)
        sendEmail(user.login, subject, html)

        return {
            ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0000' }),
            clientMessage: 'Solicitud de recuperacion exitosa.',
            systemMessage: 'Solicitud exitosa'
        }
    }
}
