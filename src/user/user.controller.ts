import { AuthReqDTO } from '../dto/auth.dto';
import {
    Controller,
    Get,
    Post,
    Body,
    Query,
    Logger,
    UseGuards,
    Req,
    Put,
    Param,
    UseFilters,
} from '@nestjs/common';
import {
    ApiBody,
    ApiTags,
    ApiQuery,
    ApiResponse,
    ApiOperation,
    ApiBearerAuth,
} from '@nestjs/swagger';
import { Request } from 'express';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { AllExceptionsFilter } from '../filters/all-exceptions.filter';
import { UserService } from './user.service';
import { LocalAuthGuard } from 'src/auth/local-auth.guard';

@ApiBearerAuth('JWT')
@ApiTags('User')
@Controller('user')
export class UserController {
    private readonly logger = new Logger(UserController.name);
    constructor(
        private readonly userService: UserService
    ) { }

    @UseFilters(AllExceptionsFilter)
    @Post('/recoverPassword')
    @ApiResponse({
        status: 200,
        description: 'Consulta exitosa',
    })
    @ApiOperation({ summary: 'Recuperar password' })
    public async getCommerce(
        @Req() req: Request & { user: AuthReqDTO },
    ): Promise<any> {
        return this.userService.recoverPassword(req.body.username);
    }

}