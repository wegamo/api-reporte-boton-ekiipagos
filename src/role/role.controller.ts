import { AuthReqDTO } from '../dto/auth.dto';
import {
    Controller,
    Post,
    Logger,
    UseGuards,
    Req,
    UseFilters,
    Get,
} from '@nestjs/common';
import {
    ApiTags,
    ApiResponse,
    ApiOperation,
    ApiBearerAuth,
} from '@nestjs/swagger';
import { Request } from 'express';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { AllExceptionsFilter } from '../filters/all-exceptions.filter';
import { RoleService } from './role.service';

@ApiBearerAuth('JWT')
@ApiTags('Role')
@Controller('role')
export class RoleController {
    private readonly logger = new Logger(RoleController.name);
    constructor(
        private readonly roleService: RoleService
    ) { }

    @UseGuards(JwtAuthGuard)
    @UseFilters(AllExceptionsFilter)
    @Get()
    @ApiResponse({
        status: 200,
        description: 'Consulta exitosa',
    })
    @ApiOperation({ summary: 'Recuperar password' })
    public async getRoles(
        @Req() req: Request & { user: AuthReqDTO },
    ): Promise<any> {
        return this.roleService.find(/* req.body.username */);
    }

}