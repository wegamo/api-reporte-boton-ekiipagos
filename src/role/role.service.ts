import { Role } from '../model/role.entity';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { RoleI } from '../interfaces/role.interface';
import { HANDLER_RESPONSE } from 'src/utils/handler-response.utils';

@Injectable()
export class RoleService {
    constructor(
        @InjectRepository(Role)
        private readonly roleModel: Repository<Role>
    ) { }

    public async find(): Promise<any> {
        const roleList = await this.roleModel.find()
        return {
            ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0000' }), roleList
        };
    }

    public async findOne(id: number): Promise<Role> {
        return this.roleModel.findOne({ where: { id } })
    }

    public async create(data: RoleI): Promise<Role> {
        return this.roleModel.save(data)
    }
}