const moment = require('moment')


const generateData = (transactions): string => {
    let fileData = '';

    fileData += 'ID de transaccion, Banco,Referencia bancaria, Fecha, Estado, Comercio,Plataforma, Correo electronico, Numero de telefono, Nombre ,Monto  \n';
    transactions.forEach(transaction => {
        fileData += `${transaction.reference},${transaction.bank ? transaction.bank : '-'},${transaction.external_reference ? transaction.external_reference : '-'},${moment(transaction.date).format('DD-MM-YYYY')},${transaction.status},${transaction.id_platform_commerce.id_commerce.name},${transaction.id_platform_commerce.id_payment_platform.name},${transaction.id_order.id_customer.email},${transaction.id_order.id_customer.phone},${transaction.id_order.id_customer.name && transaction.id_order.id_customer.name != 0 ? transaction.id_order.id_customer.name : '-'},${transaction.id_order.amount} \n`;

    });

    return fileData;
}

export const generateFile = (transactions) => {

    const file = generateData(transactions)

    return file;

}   