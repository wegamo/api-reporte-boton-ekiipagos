import { ResponseDTO } from '../dto/response.dto';
import { SERVER_ERROR } from './error.utils';

const HANDLER_RESPONSE = {
    RESPONSE: (response: Partial<any>): ResponseDTO => {
        switch (response.codigoRespuesta) {
            case '0000':

                return SERVER_ERROR['00'];
            case '0002':

                return SERVER_ERROR['01'];
            case 'E010':

                return SERVER_ERROR['02'];
            case 'E012':

                return SERVER_ERROR['03'];
            case 'E013':

                return SERVER_ERROR['04'];
            case 'E014':

                return SERVER_ERROR['05'];
            case 'E015':

                return SERVER_ERROR['06'];
            case 'E016':

                return SERVER_ERROR['04'];
            case 'E017':

                return SERVER_ERROR['05'];
            case 'E018':

                return SERVER_ERROR['07'];

            case 'E021':
                return SERVER_ERROR['08'];

            case 'E022':
                return SERVER_ERROR['09'];

            case 'E023':
                return SERVER_ERROR['10'];

            case 'E024':
                return SERVER_ERROR['11'];

            case 'E030':
                return SERVER_ERROR['12'];

            case '0096':
                return SERVER_ERROR['13'];

            case 'A001':
                return SERVER_ERROR['14'];

            case 'A002':
                return SERVER_ERROR['15'];

            case 'A003':
                return SERVER_ERROR['16'];

            case 'A004':
                return SERVER_ERROR['17'];

            case '0103':
                return SERVER_ERROR['18'];

            case '41':
                return SERVER_ERROR['41'];

            case '40':
                return SERVER_ERROR['40'];

            case '42':
                return SERVER_ERROR['42'];

            case '43':
                return SERVER_ERROR['43'];

            default:
                return SERVER_ERROR['13'];
        }
    }
};

export {
    HANDLER_RESPONSE
};