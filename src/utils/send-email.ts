import * as nodemailer from 'nodemailer';
import * as aws from 'aws-sdk';

export const sendEmail = async (
    email: string | string[],
    subject: string,
    html: string,
    from?: string,

) => {
    aws.config.update({
        accessKeyId: process.env.AWS_KEY,
        secretAccessKey: process.env.AWS_SECRET_KEY,
        region: process.env.AWS_HOST,
    });

    const transporter = nodemailer.createTransport({
        SES: new aws.SES({
            apiVersion: '2010-12-01',
        }),
    });

    // send mail with defined transport object

    try {
        const info = await transporter.sendMail({
            from: from ? from : '"Envíos Ekiipago" <soporte@ekiipago.com',
            to: email,
            subject,
            html,
        });
        console.log("Message sent::::: %s", info.messageId);
    } catch (error) {
        console.log('Message sent Error::::: %s', error);
    }
};
