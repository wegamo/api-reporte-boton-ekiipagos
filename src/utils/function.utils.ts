const CryptoJs = require('crypto-js');

export const FUNCTIONS = {
    SIGNATURE: (data: string, key): string => {
        return CryptoJs.HmacSHA384(data, key).toString();
    },
    NONCE: (date: Date): string => {
        return (date.getTime() * 1000).toString();
    },
};
