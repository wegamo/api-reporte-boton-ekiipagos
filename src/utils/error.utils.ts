export const SERVER_ERROR = {
    '00': {
        errorCode: '00',
        clientMessage: 'Transacción Exitosa',
        systemMessage: 'Transaccion procesada',
        codigoHttp: 200
    },
    '01': {
        errorCode: '01',
        clientMessage: 'Error interno.',
        systemMessage: 'Faltan parametros',
        codigoHttp: 400
    },
    '02': {
        errorCode: '02',
        clientMessage: 'Error interno.',
        systemMessage: 'Monto invalido',
        codigoHttp: 400
    },
    '03': {
        errorCode: '03',
        clientMessage: 'Teléfono no habilitado para realizar pagos',
        systemMessage: 'Telefono no habilitado para pagar',
        codigoHttp: 400
    },
    '04': {
        errorCode: '04',
        clientMessage: 'Excede la cantidad de pagos diarios',
        systemMessage: 'Excede cantidad de pagos diarios',
        codigoHttp: 400
    },
    '05': {
        errorCode: '05',
        clientMessage: 'Excede el monto total diario',
        systemMessage: 'Excede monto total de pagos diarios',
        codigoHttp: 400
    },
    '06': {
        errorCode: '06',
        clientMessage: 'Excede el monto máximo por transaccion',
        systemMessage: 'Excede monto maximo para un pago',
        codigoHttp: 400
    },
    '07': {
        errorCode: '07',
        clientMessage: 'Número de Banco incorrecto',
        systemMessage: 'Error en numero de banco',
        codigoHttp: 400
    },
    '08': {
        errorCode: '08',
        clientMessage: 'El teléfono del beneficiario no está registrado',
        systemMessage: 'Telefono receptor no registrado',
        codigoHttp: 400
    },
    '09': {
        errorCode: '09',
        clientMessage: 'El teléfono del beneficiario no acepta pagos',
        systemMessage: 'Telefono receptor no acepta pagos',
        codigoHttp: 400
    },
    '10': {
        errorCode: '10',
        clientMessage: 'La identificación del beneficiario no coincide',
        systemMessage: 'Identifiacion del beneficiario no coincide',
        codigoHttp: 400
    },
    '11': {
        errorCode: '11',
        clientMessage: 'El cliente se encuentra bloqueado',
        systemMessage: 'Cliente bloqueado',
        codigoHttp: 400
    },
    '12': {
        errorCode: '12',
        clientMessage: 'El banco no se encuentra activo en estos momentos',
        systemMessage: 'Banco no activo en pago movil',
        codigoHttp: 400
    },
    '13': {
        errorCode: '13',
        clientMessage: 'Error Interno',
        systemMessage: 'Error en sistema',
        codigoHttp: 400
    },
    '14': {
        errorCode: '14',
        clientMessage: 'Error Interno',
        systemMessage: 'Firma digital invalida',
        codigoHttp: 400
    },
    '15': {
        errorCode: '15',
        clientMessage: 'Error Interno',
        systemMessage: 'Firma digital vencida',
        codigoHttp: 400
    },
    '16': {
        errorCode: '16',
        clientMessage: 'Error Interno',
        systemMessage: 'Recurso no autorizado',
        codigoHttp: 400
    },
    '17': {
        errorCode: '17',
        clientMessage: 'Error Interno',
        systemMessage: 'Api-Key invalida o revocada',
        codigoHttp: 400
    },
    '18': {
        errorCode: '18',
        clientMessage: 'Error consultando datos del cliente.',
        systemMessage: 'Error al consultar los datos del cliente o el cliente no existe.',
        codigoHttp: 400
    },
    '40': {
        errorCode: '40',
        clientMessage: 'Transacción rechazada, para más detalles contactenos.',
        systemMessage: 'No se recibio pago en el tiempo determinado',
        codigoHttp: 400
    },
    '41': {
        errorCode: '41',
        clientMessage: 'Error Interno',
        systemMessage: 'Credenciales invalidas',
        codigoHttp: 401
    },
    '42': {
        errorCode: '42',
        clientMessage: 'Error interno.',
        systemMessage: 'Numero de referencia invalido.',
        codigoHttp: 400
    },
    '43': {
        errorCode: '43',
        clientMessage: 'No se han recibido pagos con los datos suministrados.',
        systemMessage: 'No se han recibido pagos con el número de teléfono suministrado.',
        codigoHttp: 400
    }
}