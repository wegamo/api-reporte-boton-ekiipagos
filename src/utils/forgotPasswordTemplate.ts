export const forgotPasswordTemplate = (name: string, password: string) => `
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
    xmlns:o="urn:schemas-microsoft-com:office:office" lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <style type="text/css">
        .container {
            width: 100%;
            padding: 0 auto;
            margin: 0 auto;

        }

        .imgContainer {
            padding-top: 80px;
            text-align: center;
            padding-bottom: 20px;
            background-color: #e0e2e2;
        }

        .subTitle {
            font-size: 30px;
            font-family: Helvetica;
            line-height: 36px;
            letter-spacing: 0.3px;
            margin: 0px;
            margin-bottom: 18px;
            color: #000000 !important;
            text-decoration: none;
        }

        .tableText {
            font-family: Helvetica;
            font-size: 22px;
            line-height: 20px;
            letter-spacing: 0px;
            color: #666666;
        }

        .bold {
            font-weight: bold;
            color: #000000 !important;
        }

        .normal {
            font-weight: normal;
            color: #000000;
        }

        .footer {
            background-color: #e0e2e2;
            padding: 10px 44px 1px 44px;
            color: #707070;
        }

        .dividerLine {
            height: 1px;
            background-color: #ffffff;
            margin-top: 8px;
            margin-bottom: 8px;
        }

        .logo {
            width: 400;
        }

        .icons {
            height: 16px;

        }


        @media only screen and (max-width: 480px) {
            p .bold {
                font-size: 14px;
            }

            small .normal {
                font-size: 14px;
            }
        }
    </style>
    <title>Pago Movil Email</title>
</head>

<body>
    <div class="container">
        <div class="bodyContainer" style="padding-bottom: 30px; ">
            <div class="imgContainer">
                <img src="http://imgfz.com/i/ySUG9LZ.png" alt="Ekiiipago" class="logo" />
                <p style="font-size: 30px;color: black;font-weight: bold;font-family: Helvetica;">
                    !Hola ${name}!
                </p>
            </div>
            <table style="width: 100%; text-align: center;">
                <thead>
                    <tr>
                        <th scope="col">
                            <h1 class="subTitle" style="font-weight: bold; color:#000000">

                            </h1>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tableText">
                            <p class="bold">
                                A continuación podrás ver tu contraseña:
                            </p>
                            <p class="normal">${password}</p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <footer>
            <div class="footer" style="text-align: center;">
                <img src="http://imgfz.com/i/1e29HRn.png" alt="Ekiiipago" style="height: 19px;" />
                <div class="dividerLine"></div>
                <a style="font-size: 12px; font-weight: bold; color: #707070;" href="#">
                    Términos y Condiciones
                </a>
                <p class="mb-1" style="font-size: 11px;">
                    Para comunicarse con nosotros escríbenos a:
                    <a
                        style="font-size: 11px;text-decoration: none;color: #707070; font-weight: bold">soporte@ekiipago.com</a>
                </p>
                <div class=" mb-1">
                    <a href="#"><img src="http://imgfz.com/i/pdHSnON.png" alt="Facebook" class="icons" /></a>
                    <a href="#">
                        <img src="http://imgfz.com/i/GAzl9nr.png" alt="Instagram" class="icons" /></a>
                    <a href="#"><img src="http://imgfz.com/i/uXVjLAr.png" alt="Youtube" class="icons" /></a>
                    <a href="#">
                        <img src="http://imgfz.com/i/16vBxei.png" alt="Twitter" class="icons" /></a>
                </div>
                <p style="font-size: 10px;">
                    Todos los Derechos Reservados - Celupagos, C.A. - 2020
                </p>
            </div>
        </footer>
    </div>
</body>

</html>
`;
