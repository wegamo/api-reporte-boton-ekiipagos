import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Not, Repository } from 'typeorm';
import { UserPlatform } from '../model/user-platform.entity'
import { HANDLER_RESPONSE } from '../utils/handler-response.utils';
import { HttpService } from '@nestjs/common';
import { CommercesDTO } from '../dto/commerce.dto';
import { ResponseDTO } from '../dto/response.dto';
import { UserPlatformI } from '../interfaces/user-platform.interface';
import { JwtService } from '@nestjs/jwt'
import { generateFile } from '../utils/generateFile';
import { UserService } from 'src/user/user.service';
import { NewUserDTO, UpdateUserDTO } from 'src/dto/user.dto';
import { UserI } from 'src/interfaces/user.interface';

@Injectable()
export class UserPlatformService {
    constructor(
        @InjectRepository(UserPlatform)
        private readonly userPlatformModel: Repository<UserPlatform>,
        private readonly _http: HttpService,
        private readonly jwtService: JwtService,
        private readonly userService: UserService,
    ) { }

    public async findUserList(login: string): Promise<any> {


        const userList: any[] = []

        const users = await this.userService.find();

        const relationPlatform = await this.userPlatformModel.find({ relations: ['id_user', 'id_platform'] })

        users.forEach(user => {
            const platforms = []

            const relations = relationPlatform.filter(platform => platform.id_user.id == user.id);

            relations.forEach(element => {
                platforms.push({ id: element.id_platform.id, name: element.id_platform.name, login: element.login })
            })
            userList.push({ ...user, platforms })
        })

        return {
            ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0000' }), userList,
        };

    }

    public async findUser(login: string, id: number) {
        const users = await this.userService.findOne({ id });

        const relationPlatform = await (await this.userPlatformModel.find({ relations: ['id_user', 'id_platform'] })).filter(platform => platform.id_user.id == users.id)

        return {
            ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0000' }), user: { ...users, platforms: relationPlatform },
        };
    }

    public async updateUser(login: string, data: UpdateUserDTO) {
        const { id } = data.user;

        const updateUser: UserI = {
            name: data.user.name,
            login: data.user.login,
            password: data.user.password,
            id_role: data.user.role
        }

        return await this.userService.update(id, updateUser).then(response => {
            try {
                const platformLength = []
                data.platforms.forEach(async (elemPlatform) => {
                    platformLength.push(elemPlatform.platform.id)
                    const updatePlatform: UserPlatformI = {
                        id_user: { id, ...updateUser },
                        id_platform: elemPlatform.platform,
                        login: elemPlatform.login,
                        password: elemPlatform.password

                    }
                    const findUserPlatform = await this.userPlatformModel.findOne({ where: { id_platform: updatePlatform.id_platform, id_user: updatePlatform.id_user } })
                    if (findUserPlatform) {
                        await this.userPlatformModel.update({ id: findUserPlatform.id }, updatePlatform);
                    } else {
                        await this.create(updatePlatform)
                    }


                    await this.userPlatformModel.delete({ id_platform: { id: Not(In(platformLength)) }, id_user: { id } })
                })
            } catch (err) {
                console.log(err);
                return err
            }
            return {
                ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0000' })
            };
        }).catch(error => {
            console.log(error);
            return {
                ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0096' }), error: error.response.data
            }

        })

    }
    public async createUser(login: string, data: NewUserDTO): Promise<any> {

        const newUser: UserI = {
            name: data.user.name,
            login: data.user.login,
            password: data.user.password,
            id_role: data.user.role
        }


        return await this.userService.create(newUser).then(response => {
            try {
                data.platforms.forEach(async (platform) => {
                    const newPlatform: UserPlatformI = {
                        id_user: response,
                        id_platform: platform.platform,
                        login: platform.login,
                        password: platform.password

                    }
                    await this.create(newPlatform)
                })
            } catch (err) {
                console.log(err);

                return err
            }
            return {
                ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0000' })
            };
        })
            .catch(error => {
                console.log(error);
                return {
                    ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0096' }), error: error.response.data
                }

            })
    }

    public async deleteUser(login: string, id: number) {



        return await this.userPlatformModel.delete({ id_user: { id } }).then(async (response) => {


            return this.userService.delete(id).then((response) => {


                return {
                    ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0000' })
                };
            }).catch(error => {
                console.log(error);
                return {
                    ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0096' }), error: error.response.data
                }

            })



        })
    }
    public async findCommerce(login: string): Promise<CommercesDTO | ResponseDTO> {

        const { id } = await this.userService.findOne({ login })

        const platforms = await this.find({ id_user: id })

        const platform = platforms.find(platform => platform.id_platform.name.toLowerCase() == 'p2c')


        if (platform) {
            const token = this.jwtService.sign({ username: platform.login, password: platform.password })
            return this._http.get(`${platform.id_platform.url}/commerce`, { headers: { 'Authorization': `Bearer ${token}` } })
                .toPromise()
                .then(({ data }) => {

                    return {
                        ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0000' }), commerces: data.commerces
                    };
                })
                .catch(error => {
                    return {
                        ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0096' }), error: error.response.data
                    }
                })



        } else {
            return HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: 'A003' })

        }
    }

    public async findCommerceTDC(login: string): Promise<CommercesDTO | ResponseDTO> {

        const { id } = await this.userService.findOne({ login })

        const platforms = await this.find({ id_user: id })


        const platform = platforms.find(platform => platform.id_platform.name.toLowerCase() == 'otros')


        if (platform) {
            const token = await this._http.post(`${platform.id_platform.url}/auth/commerce`, { username: platform.login, password: platform.password }).toPromise().then(response => response.data.access_token)

            return this._http.get(`${platform.id_platform.url}/commerce`, { headers: { 'Authorization': `Bearer ${token}` } })
                .toPromise()
                .then(({ data }) => {

                    return {
                        ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0000' }), commerces: data.commerces
                    };
                })
                .catch(error => {
                    return {
                        ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0096' }), error: error.response.data
                    }
                })



        } else {
            return HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: 'A003' })

        }
    }
    public async findTransactionP2C(login: string, params: any): Promise<any | ResponseDTO> {

        const { id } = await this.userService.findOne({ login })

        const platforms = await this.find({ id_user: id })

        const platform = await platforms.find(platform => platform.id_platform.name.toLowerCase() == 'p2c' && platform.id_platform.currency.toLowerCase() == 'ves')

        if (platform) {
            const token = await this.jwtService.sign({ username: platform.login, password: platform.password })

            return this._http.get(`${platform.id_platform.url}/state/transactions`, { params, headers: { 'Authorization': `Bearer ${token}` } })
                .toPromise()
                .then(({ data }) => {
                    if (params.archivo) {
                        const archivo = generateFile(data.transactions.sort((a: any, b: any) => b.id - a.id))
                        return {
                            ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0000' }), data: archivo
                        };
                    }
                    return {
                        ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0000' }), transacciones: data.transactions, cantidad: data.cantidad
                    };
                })
                .catch(error => {
                    return {
                        ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0096' }), error: error.response.data
                    }
                })
        } else {
            return HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: 'A003' })
        }

    }
    public async findTransaction(login: string, params: any): Promise<any | ResponseDTO> {

        const { id } = await this.userService.findOne({ login })

        const platforms = await this.find({ id_user: id })

        const platform = platforms.find(platform => platform.id_platform.name.toLowerCase() == 'otros' && platform.id_platform.currency.toLowerCase() == 'usd')

        if (platform) {
            const token = await this._http.post(`${platform.id_platform.url}/auth/commerce`, { username: platform.login, password: platform.password }).toPromise().then(response => response.data.access_token)
            return this._http.get(`${platform.id_platform.url}/transaction`, {
                params, headers: { 'Authorization': `Bearer ${token}` }
            })
                .toPromise()
                .then(({ data }) => {
                    if (params.archivo) {

                        return {
                            ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0000' }), data
                        };
                    }
                    return {
                        ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0000' }), transacciones: data.transactions, cantidad: data.cantidad
                    };
                }).catch(error => {
                    console.log(error);

                    return {
                        ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0096' }), error: error.code
                    }
                })

        } else {
            return HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: 'A003' })
        }

    }
    public async findPlatforms(login: string,) {

        const { id } = await this.userService.findOne({ login })

        const platforms = await this.find({ id_user: id })

        const platform = platforms.find(platform => platform.id_platform.name.toLowerCase() == 'p2c')


        if (platform) {
            const token = this.jwtService.sign({ username: platform.login, password: platform.password })

            return this._http.get(`${platform.id_platform.url}/platform`, { headers: { 'Authorization': `Bearer ${token}` } })
                .toPromise()
                .then(({ data }) => {

                    return {
                        ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0000' }), platforms: data.platforms
                    };
                })
                .catch(error => {
                    return {
                        ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0096' }), error: error.response.data
                    }
                })



        } else {
            return HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: 'A003' })

        }
    }

    public async findPlatformsTDC(login: string,) {

        const { id } = await this.userService.findOne({ login })

        const platforms = await this.find({ id_user: id })

        const platform = platforms.find(platform => platform.id_platform.name.toLowerCase() == 'otros')


        if (platform) {
            const token = await this._http.post(`${platform.id_platform.url}/auth/commerce`, { username: platform.login, password: platform.password }).toPromise().then(response => response.data.access_token)

            return this._http.get(`${platform.id_platform.url}/platform`, { headers: { 'Authorization': `Bearer ${token}` } })
                .toPromise()
                .then(({ data }) => {

                    return {
                        ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0000' }), platforms: data.platforms
                    };
                })
                .catch(error => {
                    return {
                        ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0096' }), error: error.response.data
                    }
                })



        } else {
            return HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: 'A003' })

        }
    }
    public async findRate(login: string): Promise<CommercesDTO | ResponseDTO> {

        const { id } = await this.userService.findOne({ login })

        const platforms = await this.find({ id_user: id })

        const platform = platforms.find(platform => platform.id_platform.name.toLowerCase() == 'p2c')


        if (platform) {
            const token = this.jwtService.sign({ username: platform.login, password: platform.password })
            return this._http.get(`${platform.id_platform.url}/rate/exchangeRate`, { headers: { 'Authorization': `Bearer ${token}` } })
                .toPromise()
                .then(({ data }) => {

                    return {
                        ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0000' }), rateList: data.rateList
                    };
                })
                .catch(error => {
                    console.log(error);

                    return {
                        ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0096' }), error: error.response.data
                    }
                })



        } else {
            return HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: 'A003' })

        }
    }
    public async createRate(data: any, login: string): Promise<CommercesDTO | ResponseDTO> {

        const { id } = await this.userService.findOne({ login })

        const platforms = await this.find({ id_user: id })

        const platform = platforms.find(platform => platform.id_platform.name.toLowerCase() == 'p2c')


        if (platform) {
            const token = this.jwtService.sign({ username: platform.login, password: platform.password })
            return this._http.post(`${platform.id_platform.url}/rate/create`, data, { headers: { 'Authorization': `Bearer ${token}` } })
                .toPromise()
                .then(({ data }) => {

                    return {
                        ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0000' })
                    };
                })
                .catch(error => {
                    return {
                        ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0096' }), error: error.response.data
                    }
                })



        } else {
            return HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: 'A003' })

        }
    }

    public async find(data: any): Promise<UserPlatform[]> {
        return this.userPlatformModel.find({ where: { ...data }, relations: ['id_user', 'id_platform'] })
    }

    public async create(data: UserPlatformI): Promise<UserPlatform> {
        return this.userPlatformModel.save(data)
    }

    public async createPlatformP2C(login: string, data: any) {

        const { id } = await this.userService.findOne({ login })

        const platforms = await this.find({ id_user: id })

        const platform = platforms.find(platform => platform.id_platform.name.toLowerCase() == 'p2c')

        if (platform) {
            const token = this.jwtService.sign({ username: platform.login, password: platform.password })

            return this._http.post(`${platform.id_platform.url}/platform/create`, data, { headers: { 'Authorization': `Bearer ${token}` } })
                .toPromise()
                .then(({ data }) => {

                    return {
                        ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0000' }), platforms: data.platforms
                    };
                })
                .catch(error => {
                    return {
                        ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0096' }), error: error.response.data
                    }
                })

        } else {
            return HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: 'A003' })

        }
    }

    public async updatePlatformP2C(login: string, data: any) {

        const { id } = await this.userService.findOne({ login })

        const platforms = await this.find({ id_user: id })

        const platform = platforms.find(platform => platform.id_platform.name.toLowerCase() == 'p2c')

        if (platform) {
            const token = this.jwtService.sign({ username: platform.login, password: platform.password })

            return this._http.put(`${platform.id_platform.url}/platform/update`, data, { headers: { 'Authorization': `Bearer ${token}` } })
                .toPromise()
                .then(({ data }) => {

                    return {
                        ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0000' })
                    };
                })
                .catch(error => {
                    return {
                        ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0096' }), error: error.response.data
                    }
                })

        } else {
            return HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: 'A003' })

        }
    }
    public async deletePlatformP2C(login: string, idPlatform: number) {
        const { id } = await this.userService.findOne({ login })

        const platforms = await this.find({ id_user: id })

        const platform = platforms.find(platform => platform.id_platform.name.toLowerCase() == 'p2c')

        if (platform) {
            const token = this.jwtService.sign({ username: platform.login, password: platform.password })

            return this._http.delete(`${platform.id_platform.url}/platform/delete/${idPlatform}`, { headers: { 'Authorization': `Bearer ${token}` } })
                .toPromise()
                .then(({ data }) => {

                    return {
                        ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0000' })
                    };
                })
                .catch(error => {
                    return {
                        ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0096' }), error: error.response.data
                    }
                })

        } else {
            return HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: 'A003' })

        }
    }


    public async createCommerceP2C(login: string, data: any) {

        const { id } = await this.userService.findOne({ login })

        const platforms = await this.find({ id_user: id })

        const platform = platforms.find(platform => platform.id_platform.name.toLowerCase() == 'p2c')

        if (platform) {
            const token = this.jwtService.sign({ username: platform.login, password: platform.password })

            return this._http.post(`${platform.id_platform.url}/commerce/create`, data, { headers: { 'Authorization': `Bearer ${token}` } })
                .toPromise()
                .then(({ data }) => {

                    return {
                        ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0000' }), platforms: data.platforms
                    };
                })
                .catch(error => {
                    return {
                        ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0096' }), error: error.response.data
                    }
                })

        } else {
            return HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: 'A003' })

        }
    }

    public async updateCommerceP2C(login: string, data: any) {

        const { id } = await this.userService.findOne({ login })

        const platforms = await this.find({ id_user: id })

        const platform = platforms.find(platform => platform.id_platform.name.toLowerCase() == 'p2c')

        if (platform) {
            const token = this.jwtService.sign({ username: platform.login, password: platform.password })

            return this._http.put(`${platform.id_platform.url}/commerce/update`, data, { headers: { 'Authorization': `Bearer ${token}` } })
                .toPromise()
                .then(({ data }) => {

                    return {
                        ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0000' })
                    };
                })
                .catch(error => {
                    return {
                        ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0096' }), error: error.response.data
                    }
                })

        } else {
            return HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: 'A003' })

        }
    }
    public async deleteCommerceP2C(login: string, idPlatform: number) {
        const { id } = await this.userService.findOne({ login })

        const platforms = await this.find({ id_user: id })

        const platform = platforms.find(platform => platform.id_platform.name.toLowerCase() == 'p2c')

        if (platform) {
            const token = this.jwtService.sign({ username: platform.login, password: platform.password })

            return this._http.delete(`${platform.id_platform.url}/commerce/delete/${idPlatform}`, { headers: { 'Authorization': `Bearer ${token}` } })
                .toPromise()
                .then(({ data }) => {

                    return {
                        ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0000' })
                    };
                })
                .catch(error => {
                    return {
                        ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0096' }), error: error.response.data
                    }
                })

        } else {
            return HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: 'A003' })

        }
    }

}
