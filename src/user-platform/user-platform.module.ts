import { PlatformModule } from '../platform/platform.module';
import { UserModule } from '../user/user.module';
import { UserPlatformService } from './user-platform.service';
import { UserPlatformController } from './user-platform.controller';
import { Module, HttpModule } from '@nestjs/common';

import { TypeOrmModule } from '@nestjs/typeorm';
import { UserPlatform } from '../model/user-platform.entity'
import { JwtModule } from '@nestjs/jwt';

@Module({
    imports: [
        HttpModule,
        UserModule,
        PlatformModule,
        TypeOrmModule.forFeature([
            UserPlatform
        ]),
        JwtModule.register({
            secret: 'celupagos',
            signOptions: { expiresIn: '180s' }
        }),
    ],
    controllers: [UserPlatformController],
    providers: [
        UserPlatformService,
    ],
    exports: [UserPlatformModule, UserPlatformService]
})
export class UserPlatformModule { }
