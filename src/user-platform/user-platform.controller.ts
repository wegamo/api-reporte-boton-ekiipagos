import { AuthReqDTO } from '../dto/auth.dto';
import {
    Controller,
    Get,
    Post,
    Body,
    Query,
    Logger,
    UseGuards,
    Req,
    Put,
    Param,
    UseFilters,
    Delete,
} from '@nestjs/common';
import {
    ApiBody,
    ApiTags,
    ApiQuery,
    ApiResponse,
    ApiOperation,
    ApiBearerAuth,
} from '@nestjs/swagger';
import { Request } from 'express';
import { UserPlatformService } from './user-platform.service'
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { AllExceptionsFilter } from '../filters/all-exceptions.filter';
import { NewUserDTO, UpdateUserDTO } from 'src/dto/user.dto';

@ApiBearerAuth('JWT')
@ApiTags('UserPlatform')
@Controller('userPlatform')
export class UserPlatformController {
    private readonly logger = new Logger(UserPlatformController.name);
    constructor(
        private readonly userPlatformService: UserPlatformService
    ) { }

    @UseGuards(JwtAuthGuard)
    @UseFilters(AllExceptionsFilter)
    @Get('/Commerce')
    @ApiResponse({
        status: 200,
        description: 'Consulta exitosa',
    })
    @ApiOperation({ summary: 'Consulta de Comercios' })
    public async getCommerce(
        @Req() req: Request & { user: AuthReqDTO },
    ): Promise<any> {
        return this.userPlatformService.findCommerce(req.user.username);
    }
    @UseGuards(JwtAuthGuard)
    @UseFilters(AllExceptionsFilter)
    @Get('/CommerceTDC')
    @ApiResponse({
        status: 200,
        description: 'Consulta exitosa',
    })
    @ApiOperation({ summary: 'Consulta de Comercios' })
    public async getCommerceTDC(
        @Req() req: Request & { user: AuthReqDTO },
    ): Promise<any> {
        return this.userPlatformService.findCommerceTDC(req.user.username);
    }

    @UseGuards(JwtAuthGuard)
    @UseFilters(AllExceptionsFilter)
    @Get('/TransactionP2C')
    @ApiResponse({
        status: 200,
        description: 'Consulta exitosa',
    })
    @ApiOperation({ summary: 'Consulta de Transacciones P2C' })
    public async getTransactionsP2C(
        @Req() req: Request & { user: AuthReqDTO },
        @Query() param: any,
    ): Promise<any> {
        return this.userPlatformService.findTransactionP2C(req.user.username, param);
    }

    @UseGuards(JwtAuthGuard)
    @UseFilters(AllExceptionsFilter)
    @Get('/Transaction')
    @ApiResponse({
        status: 200,
        description: 'Consulta exitosa',
    })
    @ApiOperation({ summary: 'Consulta de Transacciones' })
    public async getTransactions(
        @Req() req: Request & { user: AuthReqDTO },
        @Query() param: any,

    ): Promise<any> {
        return this.userPlatformService.findTransaction(req.user.username, param);
    }
    @UseGuards(JwtAuthGuard)
    @UseFilters(AllExceptionsFilter)
    @Get('/Platform')
    @ApiResponse({
        status: 200,
        description: 'Consulta exitosa',
    })
    @ApiOperation({ summary: 'Consulta de Plataformas' })
    public async getPlatform(
        @Req() req: Request & { user: AuthReqDTO },
    ): Promise<any> {
        return this.userPlatformService.findPlatforms(req.user.username);
    }

    @UseGuards(JwtAuthGuard)
    @UseFilters(AllExceptionsFilter)
    @Get('/PlatformTDC')
    @ApiResponse({
        status: 200,
        description: 'Consulta exitosa',
    })
    @ApiOperation({ summary: 'Consulta de Plataformas' })
    public async getPlatformTDC(
        @Req() req: Request & { user: AuthReqDTO },
    ): Promise<any> {
        return this.userPlatformService.findPlatformsTDC(req.user.username);
    }
    @UseGuards(JwtAuthGuard)
    @UseFilters(AllExceptionsFilter)
    @Get('/rate')
    @ApiResponse({
        status: 200,
        description: 'Consulta exitosa',
    })
    @ApiOperation({ summary: 'Consulta de tasas' })
    public async getRate(
        @Req() req: Request & { user: AuthReqDTO },
    ): Promise<any> {
        return this.userPlatformService.findRate(req.user.username);
    }

    @UseGuards(JwtAuthGuard)
    @UseFilters(AllExceptionsFilter)
    @Post('/rate/create')
    @ApiResponse({
        status: 200,
        description: 'Consulta exitosa',
    })
    @ApiOperation({ summary: 'Agregar tasa de cambio' })
    public async createRate(
        @Body() data: any,
        @Req() req: Request & { user: AuthReqDTO },
    ): Promise<any> {
        return this.userPlatformService.createRate(data, req.user.username);
    }

    @UseGuards(JwtAuthGuard)
    @UseFilters(AllExceptionsFilter)
    @Get('/userList')
    @ApiResponse({
        status: 200,
        description: 'Consulta exitosa',
    })
    @ApiOperation({ summary: 'Consulta de Usuarios' })
    public async getUserList(
        @Req() req: Request & { user: AuthReqDTO }
    ): Promise<any> {
        return this.userPlatformService.findUserList(req.user.username);
    }

    @UseGuards(JwtAuthGuard)
    @UseFilters(AllExceptionsFilter)
    @Get('/user/:id')
    @ApiResponse({
        status: 200,
        description: 'Consulta exitosa',
    })
    @ApiOperation({ summary: 'Consulta de Usuarios' })
    public async getUser(
        @Param('id') id: number,
        @Req() req: Request & { user: AuthReqDTO }
    ): Promise<any> {
        return this.userPlatformService.findUser(req.user.username, id);
    }

    @UseGuards(JwtAuthGuard)
    @UseFilters(AllExceptionsFilter)
    @Post('/createUser')
    @ApiResponse({
        status: 200,
        description: 'Consulta exitosa',
    })
    @ApiOperation({ summary: 'Consulta de Usuarios' })
    public async createUser(
        @Body() data: NewUserDTO,
        @Req() req: Request & { user: AuthReqDTO }
    ): Promise<any> {
        return this.userPlatformService.createUser(req.user.username, data);
    }

    @UseGuards(JwtAuthGuard)
    @UseFilters(AllExceptionsFilter)
    @Put('/updateUser')
    @ApiResponse({
        status: 200,
        description: 'Consulta exitosa',
    })
    @ApiOperation({ summary: 'Consulta de Usuarios' })
    public async updateUser(
        @Body() data: UpdateUserDTO,
        @Req() req: Request & { user: AuthReqDTO }
    ): Promise<any> {
        return this.userPlatformService.updateUser(req.user.username, data);
    }

    @UseGuards(JwtAuthGuard)
    @UseFilters(AllExceptionsFilter)
    @Delete('/deleteUser/:id')
    @ApiResponse({
        status: 200,
        description: 'Consulta exitosa',
    })
    @ApiOperation({ summary: 'Consulta de Usuarios' })
    public async deleteUser(
        @Param('id') id: number,
        @Req() req: Request & { user: AuthReqDTO }
    ): Promise<any> {
        return this.userPlatformService.deleteUser(req.user.username, id);
    }
    ////////////////////////////////////////////////

    @UseGuards(JwtAuthGuard)
    @UseFilters(AllExceptionsFilter)
    @Post('/createPlatformP2C')
    @ApiResponse({
        status: 200,
        description: 'Consulta exitosa',
    })
    @ApiOperation({ summary: 'Crear de Plataforma' })
    public async createPlatformP2C(
        @Body() data: any,
        @Req() req: Request & { user: AuthReqDTO }
    ): Promise<any> {
        return this.userPlatformService.createPlatformP2C(req.user.username, data);
    }
    @UseGuards(JwtAuthGuard)
    @UseFilters(AllExceptionsFilter)
    @Put('/updatePlatformP2C')
    @ApiResponse({
        status: 200,
        description: 'Consulta exitosa',
    })
    @ApiOperation({ summary: 'actualizar de Plataforma' })
    public async updatePlatformP2C(
        @Body() data: any,
        @Req() req: Request & { user: AuthReqDTO }
    ): Promise<any> {
        return this.userPlatformService.updatePlatformP2C(req.user.username, data);
    }

    @UseGuards(JwtAuthGuard)
    @UseFilters(AllExceptionsFilter)
    @Delete('/deletePlatformP2C/:id')
    @ApiResponse({
        status: 200,
        description: 'Consulta exitosa',
    })
    @ApiOperation({ summary: 'eliminar de plataforma' })
    public async deletePlatformP2C(
        @Param('id') id: number,
        @Req() req: Request & { user: AuthReqDTO }
    ): Promise<any> {
        return this.userPlatformService.deletePlatformP2C(req.user.username, id);
    }
    ////////////////////
    @UseGuards(JwtAuthGuard)
    @UseFilters(AllExceptionsFilter)
    @Post('/createCommerceP2C')
    @ApiResponse({
        status: 200,
        description: 'Consulta exitosa',
    })
    @ApiOperation({ summary: 'Crear de Plataforma' })
    public async createCommerceP2C(
        @Body() data: any,
        @Req() req: Request & { user: AuthReqDTO }
    ): Promise<any> {
        return this.userPlatformService.createCommerceP2C(req.user.username, data);
    }
    @UseGuards(JwtAuthGuard)
    @UseFilters(AllExceptionsFilter)
    @Put('/updateCommerceP2C')
    @ApiResponse({
        status: 200,
        description: 'Consulta exitosa',
    })
    @ApiOperation({ summary: 'actualizar de Plataforma' })
    public async updateCommerceP2C(
        @Body() data: any,
        @Req() req: Request & { user: AuthReqDTO }
    ): Promise<any> {
        return this.userPlatformService.updateCommerceP2C(req.user.username, data);
    }

    @UseGuards(JwtAuthGuard)
    @UseFilters(AllExceptionsFilter)
    @Delete('/deleteCommerceP2C/:id')
    @ApiResponse({
        status: 200,
        description: 'Consulta exitosa',
    })
    @ApiOperation({ summary: 'eliminar de plataforma' })
    public async deleteCommerceP2C(
        @Param('id') id: number,
        @Req() req: Request & { user: AuthReqDTO }
    ): Promise<any> {
        return this.userPlatformService.deleteCommerceP2C(req.user.username, id);
    }

}
