import { Platform } from '../model/platform.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { PlatformService } from './platform.service';
import { PlatformController } from './platform.controller';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            Platform
        ])
    ],
    controllers: [PlatformController],
    providers: [PlatformService],
    exports: [PlatformService, TypeOrmModule]
})
export class PlatformModule { }
