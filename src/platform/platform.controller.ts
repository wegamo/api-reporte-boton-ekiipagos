import { AuthReqDTO } from '../dto/auth.dto';
import {
    Controller,
    Post,
    Logger,
    UseGuards,
    Req,
    UseFilters,
    Get,
} from '@nestjs/common';
import {
    ApiTags,
    ApiResponse,
    ApiOperation,
    ApiBearerAuth,
} from '@nestjs/swagger';
import { Request } from 'express';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { AllExceptionsFilter } from '../filters/all-exceptions.filter';
import { PlatformService } from './platform.service';

@ApiBearerAuth('JWT')
@ApiTags('Platforms')
@Controller('platforms')
export class PlatformController {
    private readonly logger = new Logger(PlatformController.name);
    constructor(
        private readonly platformService: PlatformService
    ) { }

    @UseGuards(JwtAuthGuard)
    @UseFilters(AllExceptionsFilter)
    @Get()
    @ApiResponse({
        status: 200,
        description: 'Consulta exitosa',
    })
    @ApiOperation({ summary: '' })
    public async getPlatforms(
        @Req() req: Request & { user: AuthReqDTO },
    ): Promise<any> {
        return this.platformService.find(/* req.body.username */);
    }

}