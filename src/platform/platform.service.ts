import { Platform } from '../model/platform.entity';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { PlatformI } from '../interfaces/platform.interface';
import { HANDLER_RESPONSE } from 'src/utils/handler-response.utils';

@Injectable()
export class PlatformService {
    constructor(
        @InjectRepository(Platform)
        private readonly platformModel: Repository<Platform>
    ) { }



    public async find(): Promise<any> {
        const platforms = await this.platformModel.find()
        return {
            ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0000' }), platforms
        };
    }

    public async findOne(data: Partial<PlatformI>): Promise<Platform> {
        return this.platformModel.findOne({ where: { ...data } })
    }

    public async create(data: PlatformI): Promise<Platform> {
        return this.platformModel.save(data)
    }
}