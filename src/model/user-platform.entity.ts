import { Platform } from './platform.entity';
import { User } from './user.entity'
import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne } from 'typeorm';

@Entity()
export class UserPlatform {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    login: string;

    @Column()
    password: string;

    @ManyToOne(type => User,
        user => user.id
    )
    id_user: User;

    @ManyToOne(type => Platform,
        platform => platform.id
    )
    id_platform: Platform;
}