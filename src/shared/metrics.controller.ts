import { PrometheusController } from "@willsoto/nestjs-prometheus";
import { Controller, Get, Res } from "@nestjs/common";
import { ApiExcludeEndpoint } from '@nestjs/swagger';
import { Response } from "express";

@Controller()
export class MetricsController extends PrometheusController {
  @ApiExcludeEndpoint()
  @Get()
  index(@Res() response: Response) {
    super.index(response);
  }
}
