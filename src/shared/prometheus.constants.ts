import type { HistogramConfiguration } from "prom-client";

const { NODE_ENV = 'development' } = process.env;
const DEFAULT_LABELS = { app: 'api-reporte-web-ekiipagos', env: NODE_ENV };
const API_HTTP_REQUEST_DURATION_SECONDS_HISTOGRAM_CONFIG: HistogramConfiguration<string> = {
  name: 'api_http_request_duration_seconds',
  help: 'Duration of HTTP requests in seconds',
  labelNames: ['method', 'route', 'code'],
};

export {
  DEFAULT_LABELS,
  API_HTTP_REQUEST_DURATION_SECONDS_HISTOGRAM_CONFIG
};