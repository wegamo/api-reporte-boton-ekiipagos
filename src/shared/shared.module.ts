import { Global, Module, Scope } from "@nestjs/common";
import { APP_INTERCEPTOR } from "@nestjs/core";
import { makeHistogramProvider, PrometheusModule } from "@willsoto/nestjs-prometheus";
import { register } from 'prom-client';

import { TransformInterceptor } from "../interceptors/transform.interceptor";
import { RequestsInterceptor } from "../interceptors/requests.interceptor";
import {
  API_HTTP_REQUEST_DURATION_SECONDS_HISTOGRAM_CONFIG,
  DEFAULT_LABELS
} from "./prometheus.constants";
import { MetricsController } from "./metrics.controller";

export const TransformInterceptorProvider = {
  provide: APP_INTERCEPTOR,
  scope: Scope.REQUEST,
  useClass: TransformInterceptor,
};
export const RequestInterceptorProvider = {
  provide: APP_INTERCEPTOR,
  scope: Scope.REQUEST,
  useClass: RequestsInterceptor,
};
const PROVIDERS = [
  TransformInterceptor,
  RequestsInterceptor,
  makeHistogramProvider(API_HTTP_REQUEST_DURATION_SECONDS_HISTOGRAM_CONFIG),
];

register.setDefaultLabels(DEFAULT_LABELS);

@Global()
@Module({
  imports: [
    PrometheusModule.register({
      path: '/metrics',
      controller: MetricsController,
      defaultMetrics: {
        enabled: true
      },
    }),
  ],
  providers: [...PROVIDERS],
  exports: [...PROVIDERS, PrometheusModule],
})
export class SharedModule {}
