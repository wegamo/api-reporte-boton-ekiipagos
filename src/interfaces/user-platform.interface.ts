import { User } from '../model/user.entity';
import { Platform } from '../model/platform.entity';
export interface UserPlatformI {

    readonly id_user: User;

    readonly id_platform: Platform;

    readonly login: string;

    readonly password: string;

};