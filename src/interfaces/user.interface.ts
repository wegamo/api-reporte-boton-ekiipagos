import { Role } from "src/model/role.entity";

export interface UserI {
    readonly name: string;

    readonly login: string;

    readonly password: string;

    readonly id_role: Role;

};