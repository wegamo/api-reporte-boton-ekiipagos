export interface SendEmailTemplateData {
    readonly order: number;

    readonly bankRef: string;

    readonly amount: number;

    readonly date: Date;

    readonly customer: string;

    readonly phone: string;

    readonly bank: string;
}
