import { TransformInterceptor } from './interceptors/transform.interceptor';
import { RequestsInterceptor } from './interceptors/requests.interceptor';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Server } from 'http';
import { Logger } from '@nestjs/common';
import { SwaggerModule, DocumentBuilder, SwaggerCustomOptions } from '@nestjs/swagger';
const helmet = require('helmet');
import { Histogram } from 'prom-client';
import { API_HTTP_REQUEST_DURATION_SECONDS_HISTOGRAM_CONFIG } from "./shared/prometheus.constants";

const histogram = new Histogram<string>(API_HTTP_REQUEST_DURATION_SECONDS_HISTOGRAM_CONFIG);

async function bootstrap() {
  let logger: Logger;

  const app = await NestFactory.create(AppModule);
  app.useGlobalInterceptors(
    new TransformInterceptor(),
    new RequestsInterceptor(histogram)
  );
  app.use(helmet());
  app.enableCors({
    origin: '*'
  });

  const options = new DocumentBuilder()
    .setTitle('API Report')
    .setDescription('')
    .setVersion('v0.0.1')
    .addTag('UserPlatform')
    .addTag('Auth')
    .addBearerAuth({
      type: 'http',
      scheme: 'bearer',
      bearerFormat: 'JWT'
    }, 'JWT')
    .build(),
    options2: SwaggerCustomOptions = {
      customCss: `
      .topbar-wrapper { justify-content: center; }
      .topbar-wrapper img {content:url('http://www.conectium.com.s3-website-us-east-1.amazonaws.com/logo_celupago.png'); width: 300px; height: auto; }
      .swagger-ui .topbar { background-color: #23232D; text-align: center }
    `,
      customSiteTitle: 'Celupagos'
    };

  const document = SwaggerModule.createDocument(app, options);

  SwaggerModule.setup('documentation', app, document, options2);


  await app.listen(parseInt(process.env.PORT, 10) || 3000)
    .then((res: Server) => {
      logger = new Logger('API REPORT WEB');
      logger.verbose(`LISTEN IN PORT ${res.address()['port']}`)
    });
}
bootstrap();