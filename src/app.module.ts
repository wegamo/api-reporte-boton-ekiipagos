import { JwtStrategy } from './auth/jwt.strategy';
import { Module, HttpModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './auth/auth.module';
import { PlatformModule } from './platform/platform.module';
import { UserModule } from './user/user.module';
import { UserPlatformModule } from './user-platform/user-platform.module';
import { ScheduleModule } from '@nestjs/schedule';
import { SharedModule, RequestInterceptorProvider, TransformInterceptorProvider } from './shared/shared.module';
import { RoleModule } from './role/role.module';
@Module({
  imports: [ScheduleModule.forRoot(),
  TypeOrmModule.forRoot(),
  ConfigModule.forRoot(),
  HttpModule.register({
    timeout: 600000,
  }),
    SharedModule,
    AuthModule,
    PlatformModule,
    UserModule,
    UserPlatformModule,
    RoleModule],
  controllers: [AppController],
  providers: [
    JwtStrategy,
    TransformInterceptorProvider,
    RequestInterceptorProvider,
  ],
})
export class AppModule {
  constructor() {
  }
}
