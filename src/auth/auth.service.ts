import { AuthReqDTO, AuthResDTO } from '../dto/auth.dto';
import { UserService } from '../user/user.service';
import { JwtService } from '@nestjs/jwt'
import { HttpException, HttpService, HttpStatus, Injectable } from '@nestjs/common';
import { HANDLER_RESPONSE } from 'src/utils/handler-response.utils';
import { UserPlatformService } from '../user-platform/user-platform.service';

@Injectable()
export class AuthService {
    constructor(
        private readonly userService: UserService,
        private readonly jwtService: JwtService,
        private readonly userPlatformService: UserPlatformService,
        private readonly _http: HttpService,
    ) { }

    public async validateUser(login: string, password: string): Promise<any> {

        const user = await this.userService.findOne({ login });

        if (!user) {
            throw new HttpException('El usuario no está registrado.', HttpStatus.BAD_REQUEST);
        }
        if (user && user.password != password) {
            throw new HttpException('Correo electrónico y contraseña introducidos no coinciden.', HttpStatus.BAD_REQUEST);
        }
        if (user && user.password === password) {
            const { password, ...result } = user;

            return result;
        }
        return null;
    }

    public async login(user: any): Promise<AuthResDTO> {

        const userName = await this.userService.findOne({ login: user.login })

        const payload = { username: user.login, sub: user.id };

        const role = await this.userService.findOne({ login: user.login });

        return !user.login ? user
            : {
                ...HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '0000' }),
                access_token: this.jwtService.sign(payload),
                role: role.id_role.name,
                name: userName.name
            }
    }
    public async tokenGeneration(user: any) {
        return this.jwtService.sign(user)
    }
}
