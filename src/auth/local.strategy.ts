import { HANDLER_RESPONSE } from '../utils/handler-response.utils';
import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { AuthService } from './auth.service';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
    constructor(
        private authService: AuthService
    ) {
        super();
    }

    public async validate(username: string, pass: string): Promise<any> {

        const user = await this.authService.validateUser(username, pass);
        if (!user) {
            return HANDLER_RESPONSE.RESPONSE({ codigoRespuesta: '41' })
        }
        return user;
    }
}