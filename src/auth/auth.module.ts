import { LocalStrategy } from './local.strategy';
import { HttpModule, Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { UserModule } from 'src/user/user.module';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport'
import { UserPlatformModule } from 'src/user-platform/user-platform.module';

@Module({
  imports: [
    HttpModule,
    UserPlatformModule,
    UserModule,
    PassportModule,
    JwtModule.register({
      secret: 'celupagos',
      signOptions: { expiresIn: '180s' }
    }),
  ],
  providers: [
    AuthService,
    LocalStrategy,
  ],
  controllers: [AuthController],
  exports: [AuthService]
})
export class AuthModule { }
