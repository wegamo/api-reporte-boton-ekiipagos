import { UserJWTI } from '../interfaces/user-jwt.interface';
import { AuthReqDTO, AuthResDTO } from '../dto/auth.dto';
import { LocalAuthGuard } from './local-auth.guard';
import {
    Controller,
    UseGuards,
    Post,
    Req,
    Res, HttpService, UseFilters
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { ApiTags, ApiBody, ApiResponse } from '@nestjs/swagger';
import { Request, Response } from 'express';
import { UserPlatformService } from '../user-platform/user-platform.service';
import { HANDLER_RESPONSE } from 'src/utils/handler-response.utils';
import { AllExceptionsFilter } from '../filters/all-exceptions.filter';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
    constructor(
        private readonly authService: AuthService,
        private readonly _http: HttpService,
        private readonly userPlatformService: UserPlatformService) { }

    @UseGuards(LocalAuthGuard)
    @UseFilters(AllExceptionsFilter)
    @ApiBody({ type: AuthReqDTO })
    @ApiResponse({
        status: 200,
        description: 'Autenticación exitosa',
        type: AuthResDTO,
    })
    @Post()
    async login(@Req() req: Request & { user: UserJWTI }, @Res() res: Response) {

        const auth = await this.authService.login(req.user);

        const userPlatform = await this.userPlatformService.find({ id_user: req.user.id })

        const platforms = userPlatform.map((platform) => ({ name: platform.id_platform.name }))

        return res.send({ ...auth, platforms })


    }
}
