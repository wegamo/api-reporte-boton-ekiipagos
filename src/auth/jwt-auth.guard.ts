import { Injectable, ExecutionContext } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import { Observable } from "rxjs";
import { InjectMetric } from "@willsoto/nestjs-prometheus";
import { Histogram } from "prom-client";

import { API_HTTP_REQUEST_DURATION_SECONDS_HISTOGRAM_CONFIG } from '../shared/prometheus.constants';


@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
  constructor(
    @InjectMetric(API_HTTP_REQUEST_DURATION_SECONDS_HISTOGRAM_CONFIG.name)
    public httpRequestDurationMicrosecondsHistogram: Histogram<string>
  ) {
    super();
  }

  canActivate(
    context: ExecutionContext
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
  
    request.endHistogramTimer = this.httpRequestDurationMicrosecondsHistogram.startTimer();

    return super.canActivate(context);
  }
}
