import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from "@nestjs/common";
import { InjectMetric } from "@willsoto/nestjs-prometheus";
import { Histogram } from "prom-client";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";

import { API_HTTP_REQUEST_DURATION_SECONDS_HISTOGRAM_CONFIG } from "../shared/prometheus.constants";

@Injectable()
export class RequestsInterceptor implements NestInterceptor {
  constructor(
    @InjectMetric(API_HTTP_REQUEST_DURATION_SECONDS_HISTOGRAM_CONFIG.name)
    public httpRequestDurationMicrosecondsHistogram: Histogram<string>
  ) {}

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const request = context.switchToHttp().getRequest();
    const response = context.switchToHttp().getResponse();

    request.endHistogramTimer = this.httpRequestDurationMicrosecondsHistogram.startTimer();

    return next.handle().pipe(
      tap(() => {
        request.endHistogramTimer?.({
          route: request.url,
          code: response.statusCode,
          method: request.method,
        });
      })
    );
  }
}
