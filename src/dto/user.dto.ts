import { Platform } from "src/model/platform.entity";
import { Role } from "src/model/role.entity";


export class NewUserDTO {

    user: {
        name: string;

        login: string;

        password: string;

        role: Role
    };

    platforms: [{
        login: string;

        password: string;

        platform: Platform;
    }];
}

export class UpdateUserDTO {

    user: {
        id: number;

        name: string;

        login: string;

        password: string;

        role: Role
    };

    platforms: [{
        login: string;

        password: string;

        platform: Platform;
    }];
}
