import { ApiProperty } from "@nestjs/swagger";

export class ResponseDTO {
    @ApiProperty()
    readonly errorCode: string;

    @ApiProperty()
    readonly clientMessage: string;

    @ApiProperty()
    readonly systemMessage: string;

    @ApiProperty()
    readonly codigoHttp: number;
};