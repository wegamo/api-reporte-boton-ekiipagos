import { ResponseDTO } from './response.dto';
import { ApiProperty } from '@nestjs/swagger';

export class CommerceDTO {
    @ApiProperty()
    readonly id: number;

    @ApiProperty()
    readonly name: string;
}

export class CommercesDTO extends ResponseDTO {
    @ApiProperty({ type: [CommerceDTO] })
    readonly commerces: CommerceDTO[];

};