import { ResponseDTO } from './response.dto';
import { ApiProperty } from '@nestjs/swagger';

export class AuthReqDTO {
    @ApiProperty()
    username: string;

    @ApiProperty()
    password: number;

}

export class AuthResDTO extends ResponseDTO {
    @ApiProperty()
    access_token: string;
}