import { Catch, ArgumentsHost, HttpException, HttpStatus, ExceptionFilter } from "@nestjs/common";

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  catch(exception: unknown, host: ArgumentsHost) {
    const request = host.switchToHttp().getRequest();
    const response = host.switchToHttp().getResponse();
    let statusCode = HttpStatus.INTERNAL_SERVER_ERROR;
    let error = 'Internal Server Error';
    let message = error;

    if (exception instanceof HttpException) {
      const exceptionResponse = exception.getResponse();

      if (typeof exceptionResponse === 'object') {
        ({ message = exception.message, error = message } = exceptionResponse as any);
      } else {
        message = exception.message as any;
      }

      statusCode = exception.getStatus();
    }
    
    request.endHistogramTimer?.({
      route: request.url,
      code: statusCode,
      method: request.method
    });

    response.status(statusCode).json({
      statusCode,
      message,
      error
    });
  }
}
